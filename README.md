# Magma
Magma is a repository for learning and playing around with the Vulkan API.

### Development Kits & Libraries

1. Vulkan SDK

### Building Setup

The Visual Studio Command Prompt tool is used for building the project. Settings the tool to x64, locate vcvarsall.bat and run the following command in a command prompt:
```
call "path\vcvarsall.bat" x64
```
The file can be found under `C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\`

Navigate to project folder where the `build.bat` script is located then simply type `build` in the command prompt to build the project. After building, the directory `build/` is generated.
