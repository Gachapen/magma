@echo off

REM set VK_INSTANCE_LAYERS=VK_LAYER_LUNARG_standard_validation

set CompilerFlags= -nologo -Od -Zi -EHsc -DMAGMA_SLOW=1
set WarningsDisabled= -wd4201 -wd4100 -wd4505 -wd4189 -wd4127 -wd4244
set LinkerFlags= -incremental:no -opt:ref -SUBSYSTEM:WINDOWS user32.lib

IF NOT EXIST build mkdir build
pushd build

del *.pdb > NUL 2> NUL
cl %CompilerFlags% ..\code\win32\win32_magma.cpp /link %LinkerFlags%
popd

REM Fix where spv files is retrieved from...
REM glslangValidator -V simple.vert
REM glslangValidator -V simple.frag
