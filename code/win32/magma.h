#pragma once

#if MAGMA_SLOW
#define Assert(Expression) if (!(Expression)) {*(int *)0 = 0;}
#else
#define Assert(Expression)
#endif

struct VulkanContext
{
    uint32_t width;
    uint32_t height;

    VkPhysicalDevice physicalDevice;
    VkImageView depthImageView;
    VkSurfaceKHR surface;

    VkInstance instance;
    VkDebugReportCallbackEXT callback;
};

struct Win32RenderContext
{
    VkDevice device;
    VkSwapchainKHR swapchain;
    VkQueue queue;

    VkCommandBuffer drawCommandBuffer;
    VkRenderPass renderPass;
    VkImage* presentImages;
    VkFramebuffer* framebuffers;
    VkPipeline pipeline;
    VkBuffer vertexInputBuffer;
};

struct Vertex {
    float x, y, z, w;
};