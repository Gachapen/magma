#pragma once

#define VK_USE_PLATFORM_WIN32_KHR
#define VK_NO_PROTOTYPES
#include "VK/vulkan.h"

/*
    There are two types of dispatch tables the loader maintains:
        Instance Dispatch Table
            Any function that takes a VkInstance or
            VkPhysicalDevice as their first parameter
        Device Dispatch Table
            Any function that takes a VkDevice, VkQueue
            or VkCommandBuffer as their first parameter
*/

PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr = NULL;
PFN_vkCreateInstance vkCreateInstance = NULL;
PFN_vkEnumerateInstanceLayerProperties vkEnumerateInstanceLayerProperties = NULL;
PFN_vkEnumerateInstanceExtensionProperties vkEnumerateInstanceExtensionProperties = NULL;
PFN_vkEnumeratePhysicalDevices vkEnumeratePhysicalDevices = NULL;

// Device-specific function pointers (physicalDevice):
PFN_vkCreateDevice vkCreateDevice = NULL;
PFN_vkGetPhysicalDeviceFeatures vkGetPhysicalDeviceFeatures = NULL;
PFN_vkGetPhysicalDeviceFormatProperties vkGetPhysicalDeviceFormatProperties = NULL;
PFN_vkGetPhysicalDeviceImageFormatProperties vkGetPhysicalDeviceImageFormatProperties = NULL;
PFN_vkGetPhysicalDeviceMemoryProperties vkGetPhysicalDeviceMemoryProperties = NULL;
PFN_vkGetPhysicalDeviceProperties vkGetPhysicalDeviceProperties = NULL;
PFN_vkGetPhysicalDeviceQueueFamilyProperties vkGetPhysicalDeviceQueueFamilyProperties = NULL;

// Device-specific function pointers (logicalDevice):
PFN_vkAllocateCommandBuffers vkAllocateCommandBuffers = NULL;
PFN_vkAllocateMemory vkAllocateMemory = NULL;
PFN_vkBeginCommandBuffer vkBeginCommandBuffer = NULL;
PFN_vkBindBufferMemory vkBindBufferMemory = NULL;
PFN_vkBindImageMemory vkBindImageMemory = NULL;
PFN_vkCmdPipelineBarrier vkCmdPipelineBarrier = NULL;
PFN_vkCreateBuffer vkCreateBuffer = NULL;
PFN_vkCreateCommandPool vkCreateCommandPool = NULL;
PFN_vkCreateDescriptorSetLayout vkCreateDescriptorSetLayout = NULL;
PFN_vkCreateFence vkCreateFence = NULL;
PFN_vkCreateFramebuffer vkCreateFramebuffer = NULL;
PFN_vkCreateGraphicsPipelines vkCreateGraphicsPipelines = NULL;
PFN_vkCreateImage vkCreateImage = NULL;
PFN_vkCreateImageView vkCreateImageView = NULL;
PFN_vkCreatePipelineLayout vkCreatePipelineLayout = NULL;
PFN_vkCreateRenderPass vkCreateRenderPass = NULL;
PFN_vkCreateSemaphore vkCreateSemaphore = NULL;
PFN_vkCreateShaderModule vkCreateShaderModule = NULL;
PFN_vkDestroyFence vkDestroyFence = NULL;
PFN_vkDestroySemaphore vkDestroySemaphore = NULL;
PFN_vkGetBufferMemoryRequirements vkGetBufferMemoryRequirements = NULL;
PFN_vkGetDeviceQueue vkGetDeviceQueue = NULL;
PFN_vkGetImageMemoryRequirements vkGetImageMemoryRequirements = NULL;
PFN_vkMapMemory vkMapMemory = NULL;
PFN_vkResetFences vkResetFences = NULL;
PFN_vkUnmapMemory vkUnmapMemory = NULL;
PFN_vkWaitForFences vkWaitForFences = NULL;

// CommandBuffer-specific function pointers
PFN_vkCmdBeginRenderPass vkCmdBeginRenderPass = NULL;
PFN_vkCmdBindDescriptorSets vkCmdBindDescriptorSets = NULL;
PFN_vkCmdBindPipeline vkCmdBindPipeline = NULL;
PFN_vkCmdBindVertexBuffers vkCmdBindVertexBuffers = NULL;
PFN_vkCmdDraw vkCmdDraw = NULL;
PFN_vkCmdEndRenderPass vkCmdEndRenderPass = NULL;
PFN_vkCmdSetViewport vkCmdSetViewport = NULL;
PFN_vkCmdSetScissor vkCmdSetScissor = NULL;
PFN_vkEndCommandBuffer vkEndCommandBuffer = NULL;
PFN_vkResetCommandBuffer vkResetCommandBuffer = NULL;

 // VkQueue-specific
PFN_vkQueueSubmit vkQueueSubmit = NULL;

// Extensions (EXT)
PFN_vkCreateDebugReportCallbackEXT vkCreateDebugReportCallbackEXT = NULL;
PFN_vkDebugReportMessageEXT vkDebugReportMessageEXT = NULL;
PFN_vkDestroyDebugReportCallbackEXT vkDestroyDebugReportCallbackEXT = NULL;

// Windows Platform (KHR)
PFN_vkAcquireNextImageKHR vkAcquireNextImageKHR = NULL;
PFN_vkCreateSwapchainKHR vkCreateSwapchainKHR = NULL;
PFN_vkCreateWin32SurfaceKHR vkCreateWin32SurfaceKHR = NULL;
PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR vkGetPhysicalDeviceSurfaceCapabilitiesKHR = NULL;
PFN_vkGetPhysicalDeviceSurfaceFormatsKHR vkGetPhysicalDeviceSurfaceFormatsKHR = NULL;
PFN_vkGetPhysicalDeviceSurfacePresentModesKHR vkGetPhysicalDeviceSurfacePresentModesKHR = NULL;
PFN_vkGetPhysicalDeviceSurfaceSupportKHR vkGetPhysicalDeviceSurfaceSupportKHR = NULL;
PFN_vkGetSwapchainImagesKHR vkGetSwapchainImagesKHR = NULL;
PFN_vkQueuePresentKHR vkQueuePresentKHR = NULL;
