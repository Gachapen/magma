#include <windows.h>

#define VK_USE_PLATFORM_WIN32_KHR
#define VK_NO_PROTOTYPES
#include "VK/vulkan.h"

#include "magma.h"
#include "magma_vulkan.cpp"

#define internal static
#define global_variable static

// Function declarations:
internal void InitGraphicsPipeline(VkFormat colorFormat, VkImageView* presentImageViews, uint32_t imageCount);
internal char* Win32ReadFile(LPSTR filename, DWORD numberOfBytesToRead, uint32_t &fileSize);
//internal VkResult CreateShaderModule(VkDevice device, VkShaderModule &module,
//                                     uint32_t* shaderCode, size_t codeSize);

// Regular globals (for now)
global_variable bool globalRunning;
global_variable VulkanContext context;
global_variable Win32RenderContext win32RenderContext;

VKAPI_ATTR VkBool32 VKAPI_CALL
DebugReportCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objectType,
                    uint64_t object, size_t location, int32_t messageCode,
                    const char* playerPrefix, const char* pMessage, void* pUserData)
{
    OutputDebugStringA(playerPrefix);
    OutputDebugStringA(" ");
    OutputDebugStringA(pMessage);
    OutputDebugStringA("\n");
    return VK_FALSE;
}

internal void
Win32Render()
{
    VkSemaphore presentCompleteSemaphore;
    VkSemaphore renderingCompleteSemaphore;

    VkSemaphoreCreateInfo semaphoreCreateInfo = { VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO, 0, 0 };
    vkCreateSemaphore(win32RenderContext.device, &semaphoreCreateInfo, NULL, &presentCompleteSemaphore);
    vkCreateSemaphore(win32RenderContext.device, &semaphoreCreateInfo, NULL, &renderingCompleteSemaphore);

    // NOTE(thomas): Get the next available swap chain image.
    uint32_t nextImageIndex;
    VkResult aquiredNextImage = vkAcquireNextImageKHR(win32RenderContext.device, win32RenderContext.swapchain,
                                                      UINT64_MAX, presentCompleteSemaphore,
                                                      VK_NULL_HANDLE, &nextImageIndex);

    Assert(aquiredNextImage == VK_SUCCESS);

    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(win32RenderContext.drawCommandBuffer, &beginInfo);

    VkImageMemoryBarrier layoutTransitionBarrier = {};
    layoutTransitionBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    layoutTransitionBarrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
    layoutTransitionBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    layoutTransitionBarrier.oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    layoutTransitionBarrier.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    layoutTransitionBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    layoutTransitionBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    layoutTransitionBarrier.image = win32RenderContext.presentImages[nextImageIndex];
    layoutTransitionBarrier.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };

    vkCmdPipelineBarrier(win32RenderContext.drawCommandBuffer,
                         VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                         VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                         0,
                         0, NULL,
                         0, NULL,
                         1, &layoutTransitionBarrier);

    // Activate render pass:
    VkClearValue clearValue[] = { { 1.0f, 1.0f, 1.0f, 1.0f }, { 1.0, 0.0 } };
    VkRenderPassBeginInfo renderPassBeginInfo = {};
    renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassBeginInfo.renderPass = win32RenderContext.renderPass;
    renderPassBeginInfo.framebuffer = win32RenderContext.framebuffers[nextImageIndex];
    renderPassBeginInfo.renderArea = { 0, 0, context.width, context.height };
    renderPassBeginInfo.clearValueCount = 2;
    renderPassBeginInfo.pClearValues = clearValue;
    vkCmdBeginRenderPass(win32RenderContext.drawCommandBuffer, &renderPassBeginInfo,
                         VK_SUBPASS_CONTENTS_INLINE);

    vkCmdBindPipeline(win32RenderContext.drawCommandBuffer,
                      VK_PIPELINE_BIND_POINT_GRAPHICS,
                      win32RenderContext.pipeline);

    // Take care of dynamic state:
    VkViewport viewport = { 0, 0, (float)context.width, (float)context.height, 0, 1 };
    vkCmdSetViewport(win32RenderContext.drawCommandBuffer, 0, 1, &viewport);

    VkRect2D scissor = { 0, 0, context.width, context.height };
    vkCmdSetScissor(win32RenderContext.drawCommandBuffer, 0, 1, &scissor);

    // Render the triangle:
    VkDeviceSize offsets = {};
    vkCmdBindVertexBuffers(win32RenderContext.drawCommandBuffer, 0, 1,
                           &win32RenderContext.vertexInputBuffer, &offsets);

    vkCmdDraw(win32RenderContext.drawCommandBuffer,
              3,  // Vertex count
              1,  // Instance count
              0,  // First vertex
              0); // First instance

    vkCmdEndRenderPass(win32RenderContext.drawCommandBuffer);

    VkImageMemoryBarrier prePresentBarrier = {};
    prePresentBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    prePresentBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    prePresentBarrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
    prePresentBarrier.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    prePresentBarrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    prePresentBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    prePresentBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    prePresentBarrier.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
    prePresentBarrier.image = win32RenderContext.presentImages[nextImageIndex];

    vkCmdPipelineBarrier(win32RenderContext.drawCommandBuffer,
                        VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                        VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                        0,
                        0, NULL,
                        0, NULL,
                        1, &prePresentBarrier);

    vkEndCommandBuffer(win32RenderContext.drawCommandBuffer);

    // Present:
    VkFence renderFence;
    VkFenceCreateInfo fenceCrateInfo = {};
    fenceCrateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    vkCreateFence(win32RenderContext.device, &fenceCrateInfo, NULL, &renderFence);

    VkPipelineStageFlags waitStageMash = { VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT };
    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &presentCompleteSemaphore;
    submitInfo.pWaitDstStageMask = &waitStageMash;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &win32RenderContext.drawCommandBuffer;
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &renderingCompleteSemaphore;

    vkQueueSubmit(win32RenderContext.queue, 1, &submitInfo, renderFence);

    vkWaitForFences(win32RenderContext.device, 1, &renderFence, VK_TRUE, UINT64_MAX);
    vkDestroyFence(win32RenderContext.device, renderFence, NULL);

    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = &renderingCompleteSemaphore;
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = &win32RenderContext.swapchain;
    presentInfo.pImageIndices = &nextImageIndex;

    VkResult queuePresented = vkQueuePresentKHR(win32RenderContext.queue, &presentInfo);
    Assert(queuePresented == VK_SUCCESS);

    vkDestroySemaphore(win32RenderContext.device, presentCompleteSemaphore, NULL);
    vkDestroySemaphore(win32RenderContext.device, renderingCompleteSemaphore, NULL);
}

internal
LRESULT CALLBACK
Win32MainWindowCallback(HWND window, UINT message, WPARAM wParam, LPARAM lParam)
{
    LRESULT result = 0;

    switch(message) {
        case WM_CLOSE: {
            globalRunning = false;
            break;
        }
        case WM_DESTROY: {
            globalRunning = false;
            break;
        }
        case WM_PAINT: {
            Win32Render();
            break;
        }
        default: {
            result = DefWindowProc(window, message, wParam, lParam);
        }
    }

    return result;
}

int CALLBACK
WinMain(HINSTANCE instance, HINSTANCE prevInstance,
        LPSTR commandLine, int showCode)
{

    WNDCLASS windowClass = {};
    windowClass.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
    windowClass.lpfnWndProc = Win32MainWindowCallback;
    windowClass.hInstance = instance;
    windowClass.lpszClassName = "MagmaTestClass";

    context.width = 800;
    context.height = 600;

    if (RegisterClass(&windowClass)) {
        HWND window =
            CreateWindowEx(NULL,
                           windowClass.lpszClassName,
                           "Vulkan Test",
                           WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                           100,
                           100,
                           context.width,
                           context.height,
                           NULL, NULL, instance,
                           NULL);

        if (!window) {
            return 0;
        }


        HMODULE vulkanModule = LoadLibrary("vulkan-1.dll");
        vkGetInstanceProcAddr = (PFN_vkGetInstanceProcAddr)GetProcAddress(vulkanModule, "vkGetInstanceProcAddr");
        LoadVulkanInstanceFunctions(vkGetInstanceProcAddr);

        const char *validationLayers[] = { "VK_LAYER_LUNARG_standard_validation" };
        int validationLayersCount = 1;
        bool *isLayerAvailable = NULL;

        bool isAllLayersAvailable = mvkCheckValidationLayers(validationLayers, isLayerAvailable,
                                                             validationLayersCount);

        if (!isAllLayersAvailable) {
            for (int layerIndex = 0; layerIndex < validationLayersCount; ++layerIndex) {
                if (!isLayerAvailable[layerIndex]) {
                    OutputDebugStringA("Validation layer not available: ");
                    OutputDebugStringA(validationLayers[layerIndex]);
                    OutputDebugStringA("\n");
                }
            }
            Assert(isAllLayersAvailable);
        }

        const char *extensions[] = { "VK_KHR_surface", "VK_KHR_win32_surface", "VK_EXT_debug_report" };
        int extensionsCount = 3;
        bool *extensionsAvailable = NULL;

        bool isAllExtensionsAvailable = mvkCheckExtensionProperties(NULL, extensions, extensionsAvailable,
                                                                    extensionsCount);
        
        if (!isAllExtensionsAvailable) {
            for (int layerIndex = 0; layerIndex < validationLayersCount; ++layerIndex) {
                if (!extensionsAvailable[layerIndex]) {
                    OutputDebugStringA("Validation layer not available: ");
                    OutputDebugStringA(validationLayers[layerIndex]);
                    OutputDebugStringA("\n");
                }
            }
            Assert(isAllExtensionsAvailable);
        }

        VkApplicationInfo applicationInfo = {};
        applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        applicationInfo.pApplicationName = "Vulkan Demo";
        applicationInfo.pEngineName = "Magma";
        applicationInfo.engineVersion = 1;
        applicationInfo.apiVersion = VK_MAKE_VERSION(1, 0, 0);

        VkInstanceCreateInfo instanceInfo = {};
        instanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        instanceInfo.pApplicationInfo = &applicationInfo;
        instanceInfo.enabledLayerCount = 1;
        instanceInfo.ppEnabledLayerNames = validationLayers;
        instanceInfo.enabledExtensionCount = 3;
        instanceInfo.ppEnabledExtensionNames = extensions;

        // Initialize the Vulkan Library:
        VkResult vkInstanceCreated = vkCreateInstance(&instanceInfo, NULL, &context.instance);
        Assert(vkInstanceCreated == VK_SUCCESS);

        LoadVulkanFunctions(context.instance);
        LoadVulkanExtensions(context.instance);

        // Setup platform specific callback extension:
        VkDebugReportCallbackCreateInfoEXT callbackCreateInfo = {};
        callbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
        callbackCreateInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT |
                                   VK_DEBUG_REPORT_WARNING_BIT_EXT |
                                   VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
        callbackCreateInfo.pfnCallback = &DebugReportCallback;

        VkResult vkDebugReportCallback = vkCreateDebugReportCallbackEXT(context.instance, &callbackCreateInfo, NULL, &context.callback);
        Assert(vkDebugReportCallback == VK_SUCCESS);

        // Win32 Surface
        VkWin32SurfaceCreateInfoKHR win32SurfaceCreateInfoKHR = {};
        win32SurfaceCreateInfoKHR.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
        win32SurfaceCreateInfoKHR.hinstance = instance;
        win32SurfaceCreateInfoKHR.hwnd = window;

        VkResult win32SurfaceCreated = vkCreateWin32SurfaceKHR(context.instance, &win32SurfaceCreateInfoKHR, NULL, &context.surface);
        Assert(win32SurfaceCreated == VK_SUCCESS);

        // Find graphics device
        uint32_t queueCount;
        uint32_t queueFamilyPropertyIndex;
        context.physicalDevice = mvkFindFirstPhysicalDevice(context.instance, VK_QUEUE_GRAPHICS_BIT, queueCount, queueFamilyPropertyIndex);

        VkDeviceQueueCreateInfo deviceQueueCreateInfo = {};
        deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        deviceQueueCreateInfo.queueCount = queueCount;
        deviceQueueCreateInfo.queueFamilyIndex = queueFamilyPropertyIndex;
        float queuePriorities[] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
                                    1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f };
        deviceQueueCreateInfo.pQueuePriorities = queuePriorities;

        VkBool32 supportsPresent;
        vkGetPhysicalDeviceSurfaceSupportKHR(context.physicalDevice, queueFamilyPropertyIndex,
                                             context.surface, &supportsPresent);
        Assert(supportsPresent);

        const char *deviceExtensions[] = { "VK_KHR_swapchain" };
        // NOTE(thomas):
        // In the tutorial, only shaderClipDistance was set to VK_TRUE.
        // Tough it did not check for supported features.
        // According to the spec, robustBufferAccess, may incur a 
        // run-time performance cost and one should carefully consider the 
        // implications of enabling all supported features.
        VkPhysicalDeviceFeatures physicalDeviceFeatures;
        vkGetPhysicalDeviceFeatures(context.physicalDevice, &physicalDeviceFeatures);

        VkDeviceCreateInfo deviceCreateInfo = {};
        deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        deviceCreateInfo.queueCreateInfoCount = 1;
        deviceCreateInfo.pQueueCreateInfos = &deviceQueueCreateInfo;
        deviceCreateInfo.enabledLayerCount = 1;
        deviceCreateInfo.ppEnabledLayerNames = validationLayers;
        deviceCreateInfo.enabledExtensionCount = 1;
        deviceCreateInfo.ppEnabledExtensionNames = deviceExtensions;
        deviceCreateInfo.pEnabledFeatures = &physicalDeviceFeatures;

        VkResult deviceCreated = vkCreateDevice(context.physicalDevice, &deviceCreateInfo, NULL, &win32RenderContext.device);
        Assert(deviceCreated == VK_SUCCESS);

        // Setting up command buffers

        VkCommandPoolCreateInfo commandPoolCreateInfo = {};
        commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
        commandPoolCreateInfo.queueFamilyIndex = queueFamilyPropertyIndex;

        VkCommandPool commandPool;
        VkResult commandPoolCreated = vkCreateCommandPool(win32RenderContext.device, &commandPoolCreateInfo, NULL, &commandPool);
        Assert(commandPoolCreated == VK_SUCCESS);

        VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
        commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        commandBufferAllocateInfo.commandPool = commandPool;
        commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        commandBufferAllocateInfo.commandBufferCount = 1;

        VkCommandBuffer setupCommandBuffer;
        VkResult allocatedSetupCommandBuffer = vkAllocateCommandBuffers(win32RenderContext.device, &commandBufferAllocateInfo,
                                                                        &setupCommandBuffer);
        Assert(allocatedSetupCommandBuffer == VK_SUCCESS);

        VkResult allocatedDrawCommandBuffer = vkAllocateCommandBuffers(win32RenderContext.device, &commandBufferAllocateInfo,
                                                                       &win32RenderContext.drawCommandBuffer);
        Assert(allocatedDrawCommandBuffer == VK_SUCCESS);

        // SWAPCHAIN:

        // NOTE(thomas):
        // Horrible API, would be better passing a struct with connected data, or
        // creating a class containing all vulkan context. (Missing desired image count!)
        VkFormat colorFormat;

        VkResult swapchainCreated = mvkCreateSwapchain(context.physicalDevice,
                                                          win32RenderContext.device,
                                                          context.surface,
                                                          VK_PRESENT_MODE_MAILBOX_KHR,
                                                          context.width, context.height,
                                                          colorFormat,
                                                          win32RenderContext.swapchain);
        Assert(swapchainCreated == VK_SUCCESS);

        uint32_t imageCount = 0;
        vkGetSwapchainImagesKHR(win32RenderContext.device, win32RenderContext.swapchain, &imageCount, NULL);
        win32RenderContext.presentImages = new VkImage[imageCount];
        vkGetSwapchainImagesKHR(win32RenderContext.device, win32RenderContext.swapchain,
                                &imageCount, win32RenderContext.presentImages);

        VkImageSubresourceRange presentSubresourceRange = {};
        presentSubresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        presentSubresourceRange.baseMipLevel = 0;
        presentSubresourceRange.levelCount = 1;
        presentSubresourceRange.baseArrayLayer = 0;
        presentSubresourceRange.layerCount = 1;

        // This streaches far down!
        VkImageViewCreateInfo presentImagesViewCreateInfo = {};
        presentImagesViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        presentImagesViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        presentImagesViewCreateInfo.format = colorFormat;
        presentImagesViewCreateInfo.components = { VK_COMPONENT_SWIZZLE_R,
                                                   VK_COMPONENT_SWIZZLE_G,
                                                   VK_COMPONENT_SWIZZLE_B,
                                                   VK_COMPONENT_SWIZZLE_A };
        presentImagesViewCreateInfo.subresourceRange = presentSubresourceRange;

        vkGetDeviceQueue(win32RenderContext.device, queueFamilyPropertyIndex, 0, &win32RenderContext.queue);

        VkFenceCreateInfo fenceCreateInfo = {};
        fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

        VkFence submitFence;
        vkCreateFence(win32RenderContext.device, &fenceCreateInfo, NULL, &submitFence);

        VkImageView* presentImageViews = new VkImageView[imageCount];
        for (int index = 0; index < imageCount; ++index) {

            VkSemaphore presentCompleteSemaphore;
            VkSemaphoreCreateInfo semaphoreCreateInfo = { VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO, 0, 0 };
            vkCreateSemaphore(win32RenderContext.device, &semaphoreCreateInfo, NULL, &presentCompleteSemaphore);

            uint32_t nextImageIndex;
            vkAcquireNextImageKHR(win32RenderContext.device, win32RenderContext.swapchain, UINT64_MAX,
                                  presentCompleteSemaphore, VK_NULL_HANDLE, &nextImageIndex);

            VkImageMemoryBarrier layoutTransitionBarrier = {};
            layoutTransitionBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            layoutTransitionBarrier.srcAccessMask = 0;
            layoutTransitionBarrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
            layoutTransitionBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            layoutTransitionBarrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
            layoutTransitionBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            layoutTransitionBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            layoutTransitionBarrier.image = win32RenderContext.presentImages[nextImageIndex];
            layoutTransitionBarrier.subresourceRange = presentSubresourceRange;

            VkPipelineStageFlags waitStageMash[]{ 
                VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT 
            };

            VkSubmitInfo submitInfo = {};
            submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
            submitInfo.waitSemaphoreCount = 1;
            submitInfo.pWaitSemaphores = &presentCompleteSemaphore;
            submitInfo.pWaitDstStageMask = waitStageMash;
            submitInfo.commandBufferCount = 1;
            submitInfo.pCommandBuffers = &setupCommandBuffer;
            submitInfo.signalSemaphoreCount = 0;
            submitInfo.pSignalSemaphores = NULL;

            mvkTransitionImage(win32RenderContext.device, setupCommandBuffer,
                               VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
                               layoutTransitionBarrier, submitInfo,
                               win32RenderContext.queue, submitFence);

            vkDestroySemaphore(win32RenderContext.device, presentCompleteSemaphore, NULL);
            vkResetCommandBuffer(setupCommandBuffer, 0);

            VkPresentInfoKHR presentInfo = {};
            presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
            presentInfo.waitSemaphoreCount = 0;
            presentInfo.swapchainCount = 1;
            presentInfo.pSwapchains = &win32RenderContext.swapchain;
            presentInfo.pImageIndices = &nextImageIndex;

            VkResult presentQueue = vkQueuePresentKHR(win32RenderContext.queue, &presentInfo);
            Assert(presentQueue == VK_SUCCESS);

            presentImagesViewCreateInfo.image = win32RenderContext.presentImages[index];
            VkResult imageViewCreated = vkCreateImageView(win32RenderContext.device,
                                                          &presentImagesViewCreateInfo,
                                                          NULL, &presentImageViews[index]);
            Assert(imageViewCreated == VK_SUCCESS);
        }

        // Depth Image buffer:
        VkImageCreateInfo imageCreateInfo = {};
        imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        imageCreateInfo.flags = VK_IMAGE_CREATE_SPARSE_BINDING_BIT;
        imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
        imageCreateInfo.format = VK_FORMAT_D16_UNORM;
        imageCreateInfo.extent.width = context.width;
        imageCreateInfo.extent.height = context.height;
        imageCreateInfo.extent.depth = 1;
        imageCreateInfo.mipLevels = 1;
        imageCreateInfo.arrayLayers = 1;
        imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
        imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
        imageCreateInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
        imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        imageCreateInfo.queueFamilyIndexCount = 0;
        imageCreateInfo.pQueueFamilyIndices = NULL;
        imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

        VkImage depthImage;
        VkResult imageCreated = vkCreateImage(win32RenderContext.device, &imageCreateInfo, NULL, &depthImage);
        Assert(imageCreated == VK_SUCCESS);

        VkMemoryRequirements memoryRequirements = {};
        vkGetImageMemoryRequirements(win32RenderContext.device, depthImage, &memoryRequirements);

        uint32_t imageMemoryTypeIndex = mvkGetMemoryTypeIndex(context.physicalDevice,
                                                              memoryRequirements.memoryTypeBits,
                                                              VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

        VkMemoryAllocateInfo imageAllocateInfo = {};
        imageAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        imageAllocateInfo.allocationSize = memoryRequirements.size;
        imageAllocateInfo.memoryTypeIndex = imageMemoryTypeIndex;

        VkDeviceMemory imageMemory = {};
        VkResult memoryAllocated = vkAllocateMemory(win32RenderContext.device, &imageAllocateInfo, NULL, &imageMemory);
        Assert(memoryAllocated == VK_SUCCESS);

        VkResult imageMemoryBound = vkBindImageMemory(win32RenderContext.device, depthImage, imageMemory, 0);
        Assert(imageMemoryBound == VK_SUCCESS);

        // Changing image layout - Handling depth buffer (Depth buffer initialised)
        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        VkImageSubresourceRange subresourceRange = {};
        subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
        subresourceRange.baseMipLevel = 0;
        subresourceRange.levelCount = 1;
        subresourceRange.baseArrayLayer = 0;
        subresourceRange.layerCount = 1;

        VkImageMemoryBarrier layoutTransitionBarrier = {};
        layoutTransitionBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        layoutTransitionBarrier.srcAccessMask = 0;
        layoutTransitionBarrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT |
                                                VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        layoutTransitionBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        layoutTransitionBarrier.newLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        layoutTransitionBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        layoutTransitionBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        layoutTransitionBarrier.image = depthImage;
        layoutTransitionBarrier.subresourceRange = subresourceRange;

        VkPipelineStageFlags waitStageMask[] = {
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
        };

        VkSubmitInfo submitInfo = {};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.waitSemaphoreCount = 0;
        submitInfo.pWaitSemaphores = NULL;
        submitInfo.pWaitDstStageMask = waitStageMask;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &setupCommandBuffer;
        submitInfo.signalSemaphoreCount = 0;
        submitInfo.pSignalSemaphores = NULL;

        mvkTransitionImage(win32RenderContext.device, setupCommandBuffer,
                           VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
                           layoutTransitionBarrier, submitInfo, 
                           win32RenderContext.queue, submitFence);

        vkResetCommandBuffer(setupCommandBuffer, 0);

        VkImageViewCreateInfo imageViewCreateInfo = {};
        imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        imageViewCreateInfo.image = depthImage;
        imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        imageViewCreateInfo.format = imageCreateInfo.format;
        imageViewCreateInfo.components = { VK_COMPONENT_SWIZZLE_IDENTITY,
                                           VK_COMPONENT_SWIZZLE_IDENTITY,
                                           VK_COMPONENT_SWIZZLE_IDENTITY,
                                           VK_COMPONENT_SWIZZLE_IDENTITY };
        imageViewCreateInfo.subresourceRange = subresourceRange;

        VkResult imageViewCreated = vkCreateImageView(win32RenderContext.device,
                                                      &imageViewCreateInfo, NULL,
                                                      &context.depthImageView);
        Assert(imageViewCreated == VK_SUCCESS);

        // Vertex Buffer:

        VkBufferCreateInfo vertexInputBufferInfo = {};
        vertexInputBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        vertexInputBufferInfo.size = sizeof(Vertex) * 3;
        vertexInputBufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
        vertexInputBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        VkResult bufferCreated = vkCreateBuffer(win32RenderContext.device,
                                                &vertexInputBufferInfo,
                                                NULL, &win32RenderContext.vertexInputBuffer);
        Assert(bufferCreated == VK_SUCCESS);

        VkMemoryRequirements vertexBufferMemoryRequirements = {};
        vkGetBufferMemoryRequirements(win32RenderContext.device,
                                      win32RenderContext.vertexInputBuffer,
                                      &vertexBufferMemoryRequirements);

        uint32_t bufferMemoryTypeIndex = mvkGetMemoryTypeIndex(context.physicalDevice,
                                                               vertexBufferMemoryRequirements.memoryTypeBits, 
                                                               VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

        VkMemoryAllocateInfo bufferAllocateInfo = {};
        bufferAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        bufferAllocateInfo.allocationSize = vertexBufferMemoryRequirements.size;
        bufferAllocateInfo.memoryTypeIndex = bufferMemoryTypeIndex;

        VkDeviceMemory vertexBufferMemory;
        VkResult allocatedMemory = vkAllocateMemory(win32RenderContext.device,
                                                    &bufferAllocateInfo,
                                                    NULL, &vertexBufferMemory);
        Assert(allocatedMemory == VK_SUCCESS);

        // Make Host accessible memory accesible. Using mapped memory:

        void* mapped;
        VkResult memoryMapped = vkMapMemory(win32RenderContext.device, vertexBufferMemory,
                                            0, VK_WHOLE_SIZE, 0, &mapped);
        Assert(memoryMapped == VK_SUCCESS);

        Vertex* triangle = (Vertex *)mapped;
        Vertex v1 = {  1.0f,  1.0f, 0, 1.0f };
        Vertex v2 = { -1.0f,  1.0f, 0, 1.0f };
        Vertex v3 = {  0.0f, -1.0f, 0, 1.0f };
        triangle[0] = v1;
        triangle[1] = v2;
        triangle[2] = v3;

        vkUnmapMemory(win32RenderContext.device, vertexBufferMemory);

        VkResult bufferMemoryBound = vkBindBufferMemory(win32RenderContext.device,
                                                        win32RenderContext.vertexInputBuffer,
                                                        vertexBufferMemory, 0);
        Assert(bufferMemoryBound == VK_SUCCESS);

        InitGraphicsPipeline(colorFormat, presentImageViews, imageCount);

        MSG message;

        globalRunning = true;
        while (globalRunning) {

            PeekMessage(&message, NULL, NULL, NULL, PM_REMOVE);
            if (message.message == WM_QUIT) {
                globalRunning = true;
            } else {
                TranslateMessage(&message);
                DispatchMessage(&message);
            }

            RedrawWindow(window, NULL, NULL, RDW_INTERNALPAINT);

           /* NOTE(thomas):
              Why does not this work when exiting the application?

           while (PeekMessage(&message, NULL, NULL, NULL, PM_REMOVE)) {
                switch (message.message) {
                    case WM_QUIT: {
                        globalRunning = false;
                    } break;
                    default: {
                        TranslateMessage(&message);
                        DispatchMessage(&message);
                    } break;
                }
            }
            RedrawWindow(window, NULL, NULL, RDW_INTERNALPAINT);
            */
        }

        vkDestroyDebugReportCallbackEXT(context.instance, context.callback, NULL);

        return message.wParam;
    }

    return 0;
}

internal void
InitGraphicsPipeline(VkFormat colorFormat, VkImageView* presentImageViews, uint32_t imageCount) {

    // Vertex Shader Module:
    uint32_t vertexFileSize;
    char* vertexCode = Win32ReadFile("vert.spv", 10000, vertexFileSize);

    VkShaderModule vertexShaderModule;
    VkResult vertexShaderModuleCreated = mvkCreateShaderModule(win32RenderContext.device,
                                                               vertexShaderModule,
                                                               (uint32_t *)vertexCode,
                                                               (size_t)vertexFileSize);
    Assert(vertexShaderModuleCreated == VK_SUCCESS);

    // Fragment Shader Module:
    uint32_t fragmentFileSize;
    char* fragmentCode = Win32ReadFile("frag.spv", 10000, fragmentFileSize);

    VkShaderModule fragmentShaderModule;
    VkResult fragmentShaderModuleCreated = mvkCreateShaderModule(win32RenderContext.device,
                                                                 fragmentShaderModule,
                                                                 (uint32_t *)fragmentCode,
                                                                 (size_t)fragmentFileSize);
    Assert(fragmentShaderModuleCreated == VK_SUCCESS);

    // pStages Info
    VkPipelineShaderStageCreateInfo pipelineShaderStageCreateInfo[2] = {};
    pipelineShaderStageCreateInfo[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    pipelineShaderStageCreateInfo[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
    pipelineShaderStageCreateInfo[0].module = vertexShaderModule;
    pipelineShaderStageCreateInfo[0].pName = "main";

    pipelineShaderStageCreateInfo[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    pipelineShaderStageCreateInfo[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    pipelineShaderStageCreateInfo[1].module = fragmentShaderModule;
    pipelineShaderStageCreateInfo[1].pName = "main";

    VkVertexInputBindingDescription vertexInputBindingDescription = {};
    vertexInputBindingDescription.binding = 0;
    vertexInputBindingDescription.stride = sizeof(Vertex);
    vertexInputBindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    VkVertexInputAttributeDescription vertexInputAttributeDescription = {};
    vertexInputAttributeDescription.location = 0;
    vertexInputAttributeDescription.binding = 0;
    vertexInputAttributeDescription.format = VK_FORMAT_R32G32B32A32_SFLOAT;
    vertexInputAttributeDescription.offset = 0;

    VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateCreateInfo = {};
    pipelineVertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    pipelineVertexInputStateCreateInfo.vertexBindingDescriptionCount = 1;
    pipelineVertexInputStateCreateInfo.pVertexBindingDescriptions = &vertexInputBindingDescription;
    pipelineVertexInputStateCreateInfo.vertexAttributeDescriptionCount = 1;
    pipelineVertexInputStateCreateInfo.pVertexAttributeDescriptions = &vertexInputAttributeDescription;

    VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyStateCreateInfo = {};
    pipelineInputAssemblyStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    pipelineInputAssemblyStateCreateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    pipelineInputAssemblyStateCreateInfo.primitiveRestartEnable = VK_FALSE;

    // Configuring Viewport and scissors clipping:
    VkViewport viewport = {};
    viewport.x = 0;
    viewport.y = 0;
    viewport.width = context.width;
    viewport.height = context.height;
    viewport.minDepth = 0;
    viewport.maxDepth = 1;

    VkRect2D scissors = {};
    scissors.offset = { 0, 0 };
    scissors.extent = { context.width, context.height };

    VkPipelineViewportStateCreateInfo viewportState = {};
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissors;

    VkPipelineRasterizationStateCreateInfo pipelineRasterizationStateCreateInfo = {};
    pipelineRasterizationStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    pipelineRasterizationStateCreateInfo.polygonMode = VK_POLYGON_MODE_FILL;
    pipelineRasterizationStateCreateInfo.cullMode = VK_CULL_MODE_NONE;
    pipelineRasterizationStateCreateInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    pipelineRasterizationStateCreateInfo.depthClampEnable = VK_FALSE;
    pipelineRasterizationStateCreateInfo.rasterizerDiscardEnable = VK_FALSE; // If false viewportState and multisample state is needed.
    pipelineRasterizationStateCreateInfo.depthBiasEnable = VK_FALSE;
    pipelineRasterizationStateCreateInfo.depthBiasConstantFactor = 0;
    pipelineRasterizationStateCreateInfo.depthBiasClamp = 0;
    pipelineRasterizationStateCreateInfo.depthBiasSlopeFactor = 0;
    pipelineRasterizationStateCreateInfo.lineWidth = 1;

    VkPipelineMultisampleStateCreateInfo multisampleState = {};
    multisampleState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampleState.sampleShadingEnable = VK_FALSE;
    multisampleState.minSampleShading = 0;
    multisampleState.pSampleMask = NULL;
    multisampleState.alphaToCoverageEnable = VK_FALSE;
    multisampleState.alphaToOneEnable = VK_FALSE;

    VkStencilOpState noOPStencilState = {};
    noOPStencilState.failOp = VK_STENCIL_OP_KEEP;
    noOPStencilState.passOp = VK_STENCIL_OP_KEEP;
    noOPStencilState.depthFailOp = VK_STENCIL_OP_KEEP;
    noOPStencilState.compareOp = VK_COMPARE_OP_ALWAYS;
    noOPStencilState.compareMask = 0;
    noOPStencilState.writeMask = 0;
    noOPStencilState.reference = 0;

    VkPipelineDepthStencilStateCreateInfo depthState = {};
    depthState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthState.depthTestEnable = VK_TRUE;
    depthState.depthWriteEnable = VK_TRUE;
    depthState.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
    depthState.depthBoundsTestEnable = VK_FALSE;
    depthState.stencilTestEnable = VK_FALSE;
    depthState.front = noOPStencilState;
    depthState.back = noOPStencilState;
    depthState.minDepthBounds = 0;
    depthState.maxDepthBounds = 0;

    // Color blending:
    VkPipelineColorBlendAttachmentState colorBlendingAttachmentState = {};
    colorBlendingAttachmentState.blendEnable = VK_FALSE;
    colorBlendingAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_COLOR;
    colorBlendingAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
    colorBlendingAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
    colorBlendingAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    colorBlendingAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    colorBlendingAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;
    colorBlendingAttachmentState.colorWriteMask = 0xf;

    VkPipelineColorBlendStateCreateInfo colorBlendState = {};
    colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlendState.logicOpEnable = VK_FALSE;
    colorBlendState.logicOp = VK_LOGIC_OP_CLEAR;
    colorBlendState.attachmentCount = 1;
    colorBlendState.pAttachments = &colorBlendingAttachmentState;
    colorBlendState.blendConstants[0] = 0.0;
    colorBlendState.blendConstants[1] = 0.0;
    colorBlendState.blendConstants[2] = 0.0;
    colorBlendState.blendConstants[3] = 0.0;

    // Dynamic state (viewport / scissors):
    VkDynamicState dynamicState[2] = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };

    VkPipelineDynamicStateCreateInfo dynamicStateCreateInfo = {};
    dynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicStateCreateInfo.dynamicStateCount = 2;
    dynamicStateCreateInfo.pDynamicStates = dynamicState;

    // Pipeline Layout

    // Uniform buffers for matrices:
    VkDescriptorSetLayoutBinding descriptorLayoutBinding = {};
    descriptorLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorLayoutBinding.binding = 0; // Corresponds to a resource of the same binding number in the shader stages (pStages)?
    descriptorLayoutBinding.descriptorCount = 1;
    descriptorLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    descriptorLayoutBinding.pImmutableSamplers = NULL;

    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo = {};
    descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptorSetLayoutCreateInfo.bindingCount = 1;
    descriptorSetLayoutCreateInfo.pBindings = &descriptorLayoutBinding;

    VkDescriptorSetLayout descriptorSetLayout;
    VkResult descriptorSetLayoutCreated = vkCreateDescriptorSetLayout(win32RenderContext.device, &descriptorSetLayoutCreateInfo, NULL, &descriptorSetLayout);
    Assert(descriptorSetLayoutCreated == VK_SUCCESS);

    VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {};
    pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutCreateInfo.setLayoutCount = 1;
    pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;
    pipelineLayoutCreateInfo.pushConstantRangeCount = 0;
    pipelineLayoutCreateInfo.pPushConstantRanges = NULL;

    VkPipelineLayout pipelineLayout;
    VkResult pipelineLayoutCreated = vkCreatePipelineLayout(win32RenderContext.device, &pipelineLayoutCreateInfo, NULL, &pipelineLayout);
    Assert(pipelineLayoutCreated == VK_SUCCESS);

    // Render Pass
    VkAttachmentDescription attachmentDescriptions[2] = {};
    attachmentDescriptions[0].format = colorFormat;
    attachmentDescriptions[0].samples = VK_SAMPLE_COUNT_1_BIT;
    attachmentDescriptions[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachmentDescriptions[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachmentDescriptions[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachmentDescriptions[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachmentDescriptions[0].initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    attachmentDescriptions[0].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    attachmentDescriptions[1].format = VK_FORMAT_D16_UNORM;
    attachmentDescriptions[1].samples = VK_SAMPLE_COUNT_1_BIT;
    attachmentDescriptions[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachmentDescriptions[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachmentDescriptions[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachmentDescriptions[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachmentDescriptions[1].initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    attachmentDescriptions[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentReference colorAttachmentReference = {};
    colorAttachmentReference.attachment = 0;
    colorAttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference depthAttachmentReference = {};
    depthAttachmentReference.attachment = 1;
    depthAttachmentReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpassDescription = {};
    subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpassDescription.colorAttachmentCount = 1;
    subpassDescription.pColorAttachments = &colorAttachmentReference;
    subpassDescription.pDepthStencilAttachment = &depthAttachmentReference;

    VkRenderPassCreateInfo renderPassCreateInfo = {};
    renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassCreateInfo.subpassCount = 1;
    renderPassCreateInfo.pSubpasses = &subpassDescription;
    renderPassCreateInfo.attachmentCount = 2;
    renderPassCreateInfo.pAttachments = attachmentDescriptions;
    renderPassCreateInfo.dependencyCount = 0;
    renderPassCreateInfo.pDependencies = NULL;

    VkResult renderPassCreated = vkCreateRenderPass(win32RenderContext.device, &renderPassCreateInfo,
                                                    NULL, &win32RenderContext.renderPass);
    Assert(renderPassCreated == VK_SUCCESS);

    VkImageView framebufferAttachments[2];
    framebufferAttachments[1] = context.depthImageView;

    VkFramebufferCreateInfo framebufferCreateInfo = {};
    framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebufferCreateInfo.renderPass = win32RenderContext.renderPass;
    framebufferCreateInfo.attachmentCount = 2; // Must be equal to the attachment count on render pass.
    framebufferCreateInfo.pAttachments = framebufferAttachments;
    framebufferCreateInfo.width = context.width;
    framebufferCreateInfo.height = context.height;
    framebufferCreateInfo.layers = 1;

    // Create a framebuffer per swap chain imageView:
    win32RenderContext.framebuffers = new VkFramebuffer[imageCount];
    for (uint32_t index = 0; index < imageCount; ++index) {
        framebufferAttachments[0] = presentImageViews[index];
        VkResult frameBufferCreated = vkCreateFramebuffer(win32RenderContext.device,
                                                          &framebufferCreateInfo,
                                                          NULL, &win32RenderContext.framebuffers[index]);
        Assert(frameBufferCreated == VK_SUCCESS);
    }

    // Graphics Pipline:

    VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo = {};
    graphicsPipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    graphicsPipelineCreateInfo.flags = VK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT;
    graphicsPipelineCreateInfo.stageCount = 2;
    graphicsPipelineCreateInfo.pStages = pipelineShaderStageCreateInfo;
    graphicsPipelineCreateInfo.pVertexInputState = &pipelineVertexInputStateCreateInfo;
    graphicsPipelineCreateInfo.pInputAssemblyState = &pipelineInputAssemblyStateCreateInfo;
    graphicsPipelineCreateInfo.pRasterizationState = &pipelineRasterizationStateCreateInfo;
    graphicsPipelineCreateInfo.pDepthStencilState = &depthState;
    graphicsPipelineCreateInfo.pMultisampleState = &multisampleState;
    graphicsPipelineCreateInfo.pColorBlendState = &colorBlendState;
    graphicsPipelineCreateInfo.pViewportState = &viewportState;
    graphicsPipelineCreateInfo.pDynamicState = &dynamicStateCreateInfo;
    graphicsPipelineCreateInfo.layout = pipelineLayout; // Must be consistent with all shaders specified in pStages.
    graphicsPipelineCreateInfo.renderPass = win32RenderContext.renderPass;
    graphicsPipelineCreateInfo.subpass = 0;
    graphicsPipelineCreateInfo.pTessellationState = NULL;
    graphicsPipelineCreateInfo.basePipelineHandle = NULL; // Pipeline Derivates:
    graphicsPipelineCreateInfo.basePipelineIndex = 0;

    VkResult graphicsPipelineCreated = vkCreateGraphicsPipelines(win32RenderContext.device,
                                                                 VK_NULL_HANDLE, 1,
                                                                 &graphicsPipelineCreateInfo,
                                                                 NULL, &win32RenderContext.pipeline);

    Assert(graphicsPipelineCreated == VK_SUCCESS);
}

internal char*
Win32ReadFile(LPSTR filename, DWORD numberOfBytesToRead, uint32_t &fileSize) {

    HANDLE fileHandle = CreateFileA(filename, GENERIC_READ, 0, NULL, OPEN_EXISTING,
                                    FILE_ATTRIBUTE_NORMAL, NULL);

    if (fileHandle == INVALID_HANDLE_VALUE) {
        OutputDebugStringA("Failed to open shader file.");
        exit(1);
    }

    char* fileContent = new char[numberOfBytesToRead];
    ReadFile(fileHandle, fileContent, numberOfBytesToRead, (LPDWORD)&fileSize, 0);
    CloseHandle(fileHandle);

    return fileContent;
}

