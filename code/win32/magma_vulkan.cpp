#include "magma_vulkan.h"

static void
LoadVulkanInstanceFunctions(PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr) {
    vkCreateInstance = (PFN_vkCreateInstance)vkGetInstanceProcAddr(NULL, "vkCreateInstance");
    vkEnumerateInstanceLayerProperties =
        (PFN_vkEnumerateInstanceLayerProperties)
        vkGetInstanceProcAddr(NULL, "vkEnumerateInstanceLayerProperties");
    vkEnumerateInstanceExtensionProperties =
        (PFN_vkEnumerateInstanceExtensionProperties)
        vkGetInstanceProcAddr(NULL, "vkEnumerateInstanceExtensionProperties");
}

static void
LoadVulkanExtensions(VkInstance instance)
{
    // NOTE(thomas):
    // For any commands that use a device or device-child object as their dispatable object
    // One can (should?) retrieve their function pointers with vkGetDeviceProcAddr, in cases
    // where it exists multiple vulkan implementations the function pointers returned by
    // vkGetInstanceProcAddr may point to dispatch code, may introduce overhead.

    vkCreateDebugReportCallbackEXT = (PFN_vkCreateDebugReportCallbackEXT)
        vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
    vkDestroyDebugReportCallbackEXT = (PFN_vkDestroyDebugReportCallbackEXT)
        vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
    vkDebugReportMessageEXT = (PFN_vkDebugReportMessageEXT)
        vkGetInstanceProcAddr(instance, "vkDebugReportMessageEXT");
    vkGetPhysicalDeviceSurfaceFormatsKHR = (PFN_vkGetPhysicalDeviceSurfaceFormatsKHR)
        vkGetInstanceProcAddr(instance, "vkGetPhysicalDeviceSurfaceFormatsKHR");
    vkCreateWin32SurfaceKHR = (PFN_vkCreateWin32SurfaceKHR)
        vkGetInstanceProcAddr(instance, "vkCreateWin32SurfaceKHR");
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR = (PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR)
        vkGetInstanceProcAddr(instance, "vkGetPhysicalDeviceSurfaceCapabilitiesKHR");
    vkGetPhysicalDeviceSurfacePresentModesKHR = (PFN_vkGetPhysicalDeviceSurfacePresentModesKHR)
        vkGetInstanceProcAddr(instance, "vkGetPhysicalDeviceSurfacePresentModesKHR");
    vkCreateSwapchainKHR = (PFN_vkCreateSwapchainKHR)
        vkGetInstanceProcAddr(instance, "vkCreateSwapchainKHR");
    vkGetSwapchainImagesKHR = (PFN_vkGetSwapchainImagesKHR)
        vkGetInstanceProcAddr(instance, "vkGetSwapchainImagesKHR");
    vkAcquireNextImageKHR = (PFN_vkAcquireNextImageKHR)
        vkGetInstanceProcAddr(instance, "vkAcquireNextImageKHR");
    vkQueuePresentKHR = (PFN_vkQueuePresentKHR)
        vkGetInstanceProcAddr(instance, "vkQueuePresentKHR");
    vkGetPhysicalDeviceSurfaceSupportKHR = (PFN_vkGetPhysicalDeviceSurfaceSupportKHR)
        vkGetInstanceProcAddr(instance, "vkGetPhysicalDeviceSurfaceSupportKHR");
}

static void
LoadVulkanFunctions(VkInstance instance)
{
    vkAllocateCommandBuffers = (PFN_vkAllocateCommandBuffers)
        vkGetInstanceProcAddr(instance, "vkAllocateCommandBuffers");
    vkAllocateMemory = (PFN_vkAllocateMemory)vkGetInstanceProcAddr(instance, "vkAllocateMemory");
    vkBeginCommandBuffer = (PFN_vkBeginCommandBuffer)vkGetInstanceProcAddr(instance, "vkBeginCommandBuffer");
    vkBindBufferMemory = (PFN_vkBindBufferMemory)vkGetInstanceProcAddr(instance, "vkBindBufferMemory");
    vkBindImageMemory = (PFN_vkBindImageMemory)vkGetInstanceProcAddr(instance, "vkBindImageMemory");
    vkCmdDraw = (PFN_vkCmdDraw)vkGetInstanceProcAddr(instance, "vkCmdDraw");
    vkCmdBeginRenderPass = (PFN_vkCmdBeginRenderPass)vkGetInstanceProcAddr(instance, "vkCmdBeginRenderPass");
    vkCmdBindDescriptorSets = (PFN_vkCmdBindDescriptorSets)
        vkGetInstanceProcAddr(instance, "vkCmdBindDescriptorSets");
    vkCmdBindPipeline = (PFN_vkCmdBindPipeline)vkGetInstanceProcAddr(instance, "vkCmdBindPipeline");
    vkCmdBindVertexBuffers = (PFN_vkCmdBindVertexBuffers)
        vkGetInstanceProcAddr(instance, "vkCmdBindVertexBuffers");
    vkCmdEndRenderPass = (PFN_vkCmdEndRenderPass)vkGetInstanceProcAddr(instance, "vkCmdEndRenderPass");
    vkCmdPipelineBarrier = (PFN_vkCmdPipelineBarrier)vkGetInstanceProcAddr(instance, "vkCmdPipelineBarrier");
    vkCmdSetViewport = (PFN_vkCmdSetViewport)vkGetInstanceProcAddr(instance, "vkCmdSetViewport");
    vkCmdSetScissor = (PFN_vkCmdSetScissor)vkGetInstanceProcAddr(instance, "vkCmdSetScissor");
    vkCreateBuffer = (PFN_vkCreateBuffer)vkGetInstanceProcAddr(instance, "vkCreateBuffer");
    vkCreateCommandPool = (PFN_vkCreateCommandPool)vkGetInstanceProcAddr(instance, "vkCreateCommandPool");
    vkCreateDevice = (PFN_vkCreateDevice)vkGetInstanceProcAddr(instance, "vkCreateDevice");
    vkCreateDescriptorSetLayout = (PFN_vkCreateDescriptorSetLayout)
        vkGetInstanceProcAddr(instance, "vkCreateDescriptorSetLayout");
    vkCreateFence = (PFN_vkCreateFence)vkGetInstanceProcAddr(instance, "vkCreateFence");
    vkCreateFramebuffer = (PFN_vkCreateFramebuffer)vkGetInstanceProcAddr(instance, "vkCreateFramebuffer");
    vkCreateGraphicsPipelines = (PFN_vkCreateGraphicsPipelines)
        vkGetInstanceProcAddr(instance, "vkCreateGraphicsPipelines");
    vkCreateImage = (PFN_vkCreateImage)vkGetInstanceProcAddr(instance, "vkCreateImage");
    vkCreateImageView = (PFN_vkCreateImageView)vkGetInstanceProcAddr(instance, "vkCreateImageView");
    vkCreatePipelineLayout = (PFN_vkCreatePipelineLayout)
        vkGetInstanceProcAddr(instance, "vkCreatePipelineLayout");
    vkCreateRenderPass = (PFN_vkCreateRenderPass)vkGetInstanceProcAddr(instance, "vkCreateRenderPass");
    vkCreateSemaphore = (PFN_vkCreateSemaphore)vkGetInstanceProcAddr(instance, "vkCreateSemaphore");
    vkCreateShaderModule = (PFN_vkCreateShaderModule)
        vkGetInstanceProcAddr(instance, "vkCreateShaderModule");
    vkDestroyFence = (PFN_vkDestroyFence)vkGetInstanceProcAddr(instance, "vkDestroyFence");
    vkDestroySemaphore = (PFN_vkDestroySemaphore)vkGetInstanceProcAddr(instance, "vkDestroySemaphore");
    vkEndCommandBuffer = (PFN_vkEndCommandBuffer)vkGetInstanceProcAddr(instance, "vkEndCommandBuffer");
    vkEnumeratePhysicalDevices = (PFN_vkEnumeratePhysicalDevices)
        vkGetInstanceProcAddr(instance, "vkEnumeratePhysicalDevices");
    vkGetBufferMemoryRequirements = (PFN_vkGetBufferMemoryRequirements)
        vkGetInstanceProcAddr(instance, "vkGetBufferMemoryRequirements");
    vkGetDeviceQueue = (PFN_vkGetDeviceQueue)vkGetInstanceProcAddr(instance, "vkGetDeviceQueue");
    vkGetImageMemoryRequirements = (PFN_vkGetImageMemoryRequirements)
        vkGetInstanceProcAddr(instance, "vkGetImageMemoryRequirements");
    vkGetPhysicalDeviceFeatures = (PFN_vkGetPhysicalDeviceFeatures)
        vkGetInstanceProcAddr(instance, "vkGetPhysicalDeviceFeatures");
    vkGetPhysicalDeviceFormatProperties = (PFN_vkGetPhysicalDeviceFormatProperties)
        vkGetInstanceProcAddr(instance, "vkGetPhysicalDeviceFormatProperties");
    vkGetPhysicalDeviceImageFormatProperties = (PFN_vkGetPhysicalDeviceImageFormatProperties)
        vkGetInstanceProcAddr(instance, "vkGetPhysicalDeviceImageFormatProperties");
    vkGetPhysicalDeviceMemoryProperties = (PFN_vkGetPhysicalDeviceMemoryProperties)
        vkGetInstanceProcAddr(instance, "vkGetPhysicalDeviceMemoryProperties");
    vkGetPhysicalDeviceProperties = (PFN_vkGetPhysicalDeviceProperties)
        vkGetInstanceProcAddr(instance, "vkGetPhysicalDeviceProperties");
    vkGetPhysicalDeviceQueueFamilyProperties = (PFN_vkGetPhysicalDeviceQueueFamilyProperties)
        vkGetInstanceProcAddr(instance, "vkGetPhysicalDeviceQueueFamilyProperties");
    vkMapMemory = (PFN_vkMapMemory)vkGetInstanceProcAddr(instance, "vkMapMemory");
    vkResetCommandBuffer = (PFN_vkResetCommandBuffer)vkGetInstanceProcAddr(instance, "vkResetCommandBuffer");
    vkResetFences = (PFN_vkResetFences)vkGetInstanceProcAddr(instance, "vkResetFences");
    vkUnmapMemory = (PFN_vkUnmapMemory)vkGetInstanceProcAddr(instance, "vkUnmapMemory");
    vkQueueSubmit = (PFN_vkQueueSubmit)vkGetInstanceProcAddr(instance, "vkQueueSubmit");
    vkWaitForFences = (PFN_vkWaitForFences)vkGetInstanceProcAddr(instance, "vkWaitForFences");
}

static VkResult
mvkCreateShaderModule(VkDevice device, VkShaderModule &module,
                      uint32_t* shaderCode, size_t codeSize)
{
    VkShaderModuleCreateInfo shaderInfo = {};
    shaderInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    shaderInfo.codeSize = codeSize;
    shaderInfo.pCode = shaderCode;

    return vkCreateShaderModule(device, &shaderInfo, NULL, &module);
}

static bool 
mvkCheckValidationLayers(const char* layersRequesting[], bool *&isAvailable, int requestCount)
{
    isAvailable = new bool[requestCount];

    uint32_t layersAvailableCount = 0;
    vkEnumerateInstanceLayerProperties(&layersAvailableCount, NULL);
    VkLayerProperties *layersAvailable = new VkLayerProperties[layersAvailableCount];
    vkEnumerateInstanceLayerProperties(&layersAvailableCount, layersAvailable);

    bool allAvailable = true;

    for (size_t requestIndex = 0; 
        requestIndex < requestCount;
         ++requestIndex)
    {
        int availableIndex = 0;
        bool isLayerAvailable = false;

        do {
            isLayerAvailable = (strcmp(layersRequesting[requestIndex], layersAvailable[availableIndex].layerName) == 0);

        } while (!isLayerAvailable && ++availableIndex < layersAvailableCount);

        isAvailable[requestIndex] = isLayerAvailable;
        if (!isLayerAvailable) allAvailable = false;
    }

    delete[] layersAvailable;

    return allAvailable;
}

static bool
mvkCheckExtensionProperties(const char* layerName, const char* extensionsRequesting[], 
                               bool *&isAvailable, int requestCount)
{
    isAvailable = new bool[requestCount];

    uint32_t extensionCount = 0;
    vkEnumerateInstanceExtensionProperties(layerName, &extensionCount, NULL);
    VkExtensionProperties *extensionsAvailable = new VkExtensionProperties[extensionCount];
    vkEnumerateInstanceExtensionProperties(layerName, &extensionCount, extensionsAvailable);

    bool allAvailable = true;

    for (size_t requestIndex = 0;
        requestIndex < requestCount;
        ++requestIndex)
    {
        int availableIndex = 0;
        bool isLayerAvailable = false;

        do {
            isLayerAvailable = (strcmp(extensionsRequesting[requestIndex], extensionsAvailable[availableIndex].extensionName) == 0);

        } while (!isLayerAvailable && ++availableIndex < extensionCount);

        isAvailable[requestIndex] = isLayerAvailable;
        if (!isLayerAvailable) allAvailable = false;
    }

    delete[] extensionsAvailable;

    return allAvailable;
}

static VkPhysicalDevice
mvkFindFirstPhysicalDevice(VkInstance instance, VkQueueFlagBits flagBits, 
                           uint32_t &queueCount, uint32_t &queueFamilyIndex)
{
    // Find available physical devices:
    uint32_t physicalDeviceCount = 0;
    vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, NULL);
    VkPhysicalDevice* physicalDevices = new VkPhysicalDevice[physicalDeviceCount];
    vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, physicalDevices);

    VkPhysicalDevice physicalDevice = NULL;
    uint32_t queueFamilyPropertyIndex = -1;

    uint32_t physicalDeviceIndex = 0;
    bool graphicDeviceFound = false;

    while (!graphicDeviceFound && physicalDeviceIndex < physicalDeviceCount) {

        physicalDevice = physicalDevices[physicalDeviceIndex++];

        uint32_t queueFamilyPropertyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, NULL);
        VkQueueFamilyProperties* queueFamilyProperties = new VkQueueFamilyProperties[queueFamilyPropertyCount];
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, queueFamilyProperties);

        while (!graphicDeviceFound && ++queueFamilyPropertyIndex < queueFamilyPropertyCount) {

            if (queueFamilyProperties[queueFamilyPropertyIndex].queueFlags & flagBits) {
                graphicDeviceFound = true;
                queueCount = queueFamilyProperties[queueFamilyPropertyIndex].queueCount;
                queueFamilyIndex = queueFamilyPropertyIndex;
            }
        }
    }

    delete[] physicalDevices;

    return physicalDevice;
}

static uint32_t
mvkGetMemoryTypeIndex(VkPhysicalDevice device, uint32_t memoryTypeBits, 
                      VkMemoryPropertyFlags desiredFlags)
{
    uint32_t memoryTypeIndex;

    VkPhysicalDeviceMemoryProperties memoryProperties;
    vkGetPhysicalDeviceMemoryProperties(device, &memoryProperties);

    for (uint32_t index = 0; index < 32; ++index) {
        VkMemoryType memoryType = memoryProperties.memoryTypes[index];
        if (memoryTypeBits & 1) {
            if ((memoryType.propertyFlags & desiredFlags) == desiredFlags)
            {
                memoryTypeIndex = index;
                break;
            }
        }

        memoryTypeBits = memoryTypeBits >> 1;
    }

    return memoryTypeIndex;
}

static VkResult
mvkCreateSwapchain(VkPhysicalDevice physicalDevice, VkDevice device, VkSurfaceKHR surface, 
                   VkPresentModeKHR presentModeRequested, uint32_t &width, uint32_t &height, 
                   VkFormat &colorFormat, VkSwapchainKHR &swapchain)
{
    uint32_t surfaceFormatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &surfaceFormatCount, NULL);
    VkSurfaceFormatKHR* surfaceFormats = new VkSurfaceFormatKHR[surfaceFormatCount];
    VkResult retrievedSurfaceFormat = vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface,                         // Error can happen here.
                                                                           &surfaceFormatCount, surfaceFormats);

    colorFormat = (surfaceFormats[0].format == VK_FORMAT_UNDEFINED) ? VK_FORMAT_B8G8R8A8_UNORM : surfaceFormats[0].format;

    VkColorSpaceKHR colorSpace = surfaceFormats[0].colorSpace;
    delete[] surfaceFormats;

    VkSurfaceCapabilitiesKHR surfaceCapabilities = {};
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &surfaceCapabilities);

    uint32_t desiredImageCount = 2;
    if (desiredImageCount < surfaceCapabilities.minImageCount) {
        desiredImageCount = surfaceCapabilities.minImageCount;
    } else if (surfaceCapabilities.maxImageCount != 0 &&
               desiredImageCount > surfaceCapabilities.maxImageCount)
    {
        desiredImageCount = surfaceCapabilities.maxImageCount;
    }

    VkExtent2D surfaceResolution = surfaceCapabilities.currentExtent;
    if (surfaceResolution.width == -1) {
        surfaceResolution.width = width;
        surfaceResolution.height = height;
    } else {
        width = surfaceResolution.width;
        height = surfaceResolution.height;
    }

    VkSurfaceTransformFlagBitsKHR preTransform = surfaceCapabilities.currentTransform;
    if (surfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
        preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    }

    uint32_t presentModeCount = 0;
    vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, NULL);            // Error may occur here!
    VkPresentModeKHR* presentModes = new VkPresentModeKHR[presentModeCount];
    vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, presentModes);    // Error may occur here!

    // This is the only value of presentMode that is required to be supported. Hence the default option.
    VkPresentModeKHR presentMode = VK_PRESENT_MODE_FIFO_KHR;                                                // Should not do this.

    for (uint32_t presentModeIndex = 0;
        presentModeIndex < presentModeCount;
        ++presentModeIndex)
    {
        if (presentModes[presentModeIndex] == presentModeRequested) {
            presentMode = presentModeRequested;
            break;
        }
    }

    delete[] presentModes;

    VkSwapchainCreateInfoKHR swapchainCreateInfo = {};
    swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapchainCreateInfo.surface = surface;
    swapchainCreateInfo.minImageCount = desiredImageCount;
    swapchainCreateInfo.imageFormat = colorFormat;
    swapchainCreateInfo.imageColorSpace = colorSpace;
    swapchainCreateInfo.imageExtent = surfaceResolution;
    swapchainCreateInfo.imageArrayLayers = 1;
    swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    swapchainCreateInfo.preTransform = preTransform;
    swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swapchainCreateInfo.presentMode = presentMode;
    swapchainCreateInfo.clipped = true;

    return vkCreateSwapchainKHR(device, &swapchainCreateInfo, NULL, &swapchain);
}

// Device
// VkCommandBuffer
// VkCommandBufferUsageFlags
// VkImageMemoryBarrier
// VkSubmitInfo
// VkQueue

static void
mvkTransitionImage(VkDevice device, VkCommandBuffer commandBuffer, 
                   VkCommandBufferUsageFlags usageFlags,
                   VkImageMemoryBarrier &imageMemoryBarrier, 
                   VkSubmitInfo &submitInfo, VkQueue queue, VkFence &fence)
{

    VkCommandBufferBeginInfo commandBufferBeginInfo = {};
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    commandBufferBeginInfo.flags = usageFlags;

    VkResult isRecording = vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);

    if (isRecording == VK_SUCCESS) {

        vkCmdPipelineBarrier(commandBuffer,
            VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
            VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
            0,
            0, NULL,
            0, NULL,
            1, &imageMemoryBarrier);

        vkEndCommandBuffer(commandBuffer);

        VkFenceCreateInfo fenceCreateInfo = {};
        fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

        VkResult queueSubmitted = vkQueueSubmit(queue, 1, &submitInfo, fence);
        if (queueSubmitted == VK_SUCCESS) {
            vkWaitForFences(device, 1, &fence, VK_TRUE, UINT64_MAX);
            vkResetFences(device, 1, &fence);
        }
    }
}